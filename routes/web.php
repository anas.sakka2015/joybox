<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home.index');

Route::resource('projects', ProjectController::class);
Route::put('projects/is_done/{project}', [ProjectController::class, 'isDone'])->name('projects.is_done');

Route::resource('tasks', TaskController::class);
Route::get('tasks/change_status/{task}', [TaskController::class, 'changeStatus'])->name('tasks.changeStatus');
Route::put('tasks/change_status/{task}', [TaskController::class, 'updateStatus'])->name('tasks.updateStatus');

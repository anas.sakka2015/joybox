<?php

namespace App\Enums;

enum TaskStatusEnum: string
{
    case New = 'new';
    case InProgress = 'in_progress';
    case Done = 'done';
    case Cancelled = 'canceled';
    case OnHold = 'on_hold';
}

<?php


namespace App\Constants;

use Carbon\Carbon;

class TaskStatusConstant
{

    const NEW = 'new';
    const IN_PROGRESS = 'in_progress';
    const DONE = 'done';
    const CANCELED = 'canceled';
    const ON_HOLD = 'on_hold';
}

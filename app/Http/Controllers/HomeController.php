<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $projects = Project::count();
        $tasks = Task::count();

        $year = date('Y');
        $statuses = ['done', 'in_progress', 'canceled'];
        $monthKey = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        foreach ($statuses as $status) {
            $result[$status] = Task::query()->where('status', $status)
                ->selectRaw('COUNT(*) as count')
                ->selectRaw('MONTH(created_at) as month')
                ->whereYear('created_at', $year)
                ->groupBy('month')
                ->pluck('count', 'month')
                ->toArray();


            for ($month = 1; $month <= 12; $month++) {
                $result[$status][$month] = $result[$status][$month] ?? 0;
            }
            ksort($result[$status]);
            foreach ($monthKey as $key => $month) {
                $taskStatistics[$status][$month] = $result[$status][$key + 1];
            }
        }
        $projectsPercents = Project::all();
        return view('dashboard.index', compact('projects', 'tasks', 'taskStatistics', 'projectsPercents'));
    }
}

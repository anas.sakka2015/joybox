<?php

namespace App\Http\Controllers;

use App\Constants\TaskStatusConstant;
use App\Enums\TaskStatusEnum;
use App\Http\Requests\StoreProjectRequest;
use App\Models\Task;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Requests\UpdateTaskStatusRequest;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        Paginator::useBootstrap();

        if ($request->ajax()) {
            $data = Task::with('project')->select('*');

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) use ($request) {
                    $routeName = strtok($request->route()->getName(), '.');

                    return view('includes.actions', compact('row', 'routeName'));
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('dashboard.tasks.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $projects = Project::all();

        return view('dashboard.tasks.create', compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTaskRequest $request)
    {
        $data = $request->validated();
        $data['status'] = TaskStatusConstant::NEW;
        Task::create($data);

        return to_route('tasks.index')->with('success', 'Created Successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        $data = $task->loadMissing('project');

        return view('dashboard.tasks.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Task $task)
    {
        $data = $task->loadMissing('project');
        $projects = Project::all();

        return view('dashboard.tasks.edit', compact('data', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {

        $task->update($request->validated());

        return to_route('tasks.index')->with('success', 'Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        $task->delete();

        return response()->json([
            'message' => 'Successfully deleted!',
        ], 200);
    }
    public function changeStatus(Task $task)
    {
        $data = $task;
        $status = array_column(TaskStatusEnum::cases(), 'value');

        if (in_array($task->status, [TaskStatusConstant::DONE, TaskStatusConstant::CANCELED]))
            $status = [];

        return view('dashboard.tasks.components.changeStatusForm', compact('data', 'status'));
    }
    public function updateStatus(Task $task, UpdateTaskStatusRequest $request)
    {
        $task->update($request->validated());
        $totalTasks = Project::find($task->project_id)->tasks()->count();
        $completedTasks = Project::find($task->project_id)->tasks()->where('status', 'done')->count();

        $data['percent'] = ($completedTasks / $totalTasks) * 100;
        if ($data['percent'] == 100)
            $data['is_done'] = true;
        else
            $data['is_done'] = false;

        $task->project()->update($data);

        return response()->json([
            'message' => 'Status changed successfully !',
            'data' => $task->fresh(),
        ], 200);
    }
}

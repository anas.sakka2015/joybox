<?php

namespace App\Http\Controllers;

use App\Constants\TaskStatusConstant;
use App\Models\Project;
use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Yajra\DataTables\DataTables;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        Paginator::useBootstrap();

        if ($request->ajax()) {
            $data = Project::with('tasks')->select('projects.*');

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) use ($request) {
                    $routeName = strtok($request->route()->getName(), '.');

                    return view('includes.actions', compact('row', 'routeName'));
                })
                ->addColumn('tasks_count', function ($row) use ($request) {
                    return   $row->tasks()->count();
                })
                ->rawColumns(['action', 'tasks_count'])
                ->make(true);
        }

        return view('dashboard.projects.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProjectRequest $request)
    {

        Project::create($request->validated());

        return to_route('projects.index')->with('success', 'Created Successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Project $project)
    {
        $data = $project->loadCount('tasks');

        return view('dashboard.projects.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Project $project)
    {
        $data = $project;

        return view('dashboard.projects.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {

        $project->update($request->validated());

        return to_route('projects.index')->with('success', 'Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {

        $project->delete();

        return response()->json([
            'message' => 'Successfully deleted!',
        ], 200);
    }

    /**
     * Activate the specified resource from storage.
     */
    public function isDone(Project $project)
    {

        $project->update([
            'is_done' => !$project->is_done,
        ]);

        return response()->json([
            'message' => $project->is_done ? 'Successfully Done!' : 'Successfully Un Done!',
        ], 200);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'description',
        'percent',
        'is_done'
    ];
    //Related with tasks
    public function tasks(): HasMany
    {

        return $this->hasMany(Task::class);
    }
}

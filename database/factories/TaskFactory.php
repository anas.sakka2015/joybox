<?php

namespace Database\Factories;

use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->name(),
            'description' => $this->faker->text(),
            'status' => $this->faker->randomElement(['new', 'in_progress', 'done', 'canceled', 'on_hold']),
            'deadline' => $this->faker->date(),
            'project_id' => $this->faker->randomElement(Project::all())['id'],
        ];
    }
}

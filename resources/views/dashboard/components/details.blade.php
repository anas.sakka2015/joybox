@extends('app')
@section('title', $form_title)
@section('content')
    <div class="dashboard-page-content">
        <div class="row">
            <div class="{{ isset($action['gallery']) ? 'col-4' : 'col-8' }}">
                <h2 class="fs-24 mb-2">{!! $details_title !!}</h2>
            </div>
            <div class="{{ isset($action['gallery']) ? 'col-8' : 'col-4' }} my-auto-parent ">
                <nav class="my-auto-child">
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                            role="tab" aria-controls="nav-home"
                            aria-selected="true">{{ trans('dashboard.actions.main_info') }}</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                            role="tab" aria-controls="nav-profile"
                            aria-selected="false">{{ trans('dashboard.actions.additional_info') }}</a>

                    </div>
                </nav>



            </div>
        </div>

        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                @yield('details')
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="card rounded-xl mb-5">
                    <div class="card-header p-4 bg-transparent">
                        <h4 class="fs-18 mb-0  font-weight-500">{{ trans('dashboard.actions.additional_info') }}</h4>
                    </div>
                    <header class="card-header bg-transparent p-4">
                        <div class="row align-items-center">
                            <div class="col-md-4 mb-lg-0 mb-3">
                                <span class="d-inline-block text-orange"><i
                                        class="far fa-calendar mr-1 "></i>{{ trans('dashboard.inputs.created_at') }}
                                    :
                                </span>
                                <br>
                                <small class="text-muted">{{ date('Y-m-d', strtotime($data->created_at)) }}</small>
                            </div>

                            <div class="col-md-4 mb-lg-0 mb-3">
                                <span class="d-inline-block text-orange"><i
                                        class="far fa-calendar mr-1"></i>{{ trans('dashboard.inputs.updated_at') }} :
                                </span>
                                <br>
                                <small class="text-muted">{{ date('Y-m-d', strtotime($data->updated_at)) }}</small>

                            </div>
                            @if ($data->deleted_at)
                                <div class="col-md-4 mb-lg-0 mb-3">
                                    <span class="d-inline-block text-orange"><i
                                            class="far fa-calendar mr-1"></i>{{ trans('dashboard.inputs.deleted_at') }} :
                                    </span>
                                    <br>
                                    <small class="text-muted">{{ date('Y-m-d', strtotime($data->deleted_at)) }}</small>
                                </div>
                            @endif

                        </div>
                    </header>
                </div>
            </div>
        </div>
        <div class="text-right">
            <a href="{{ $action['back'] }}" class="btn btn-primary">{{ $table_title }}
                {{ trans('dashboard.pages.list') }}</a>
        </div>
    </div>
@endsection

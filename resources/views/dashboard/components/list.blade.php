@extends('app')
@section('title', $table_title)
@section('content')
    <div class="dashboard-page-content">
        <div class="row mb-6 align-items-center">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <h2 class="fs-24 mb-0">{{ $table_title . ' ' . trans('dashboard.pages.list') }}</h2>
                {{-- <p class="mb-0">{{ $table_description }}</p> --}}
            </div>
            <div class="col-sm-6 d-flex flex-wrap justify-content-sm-end">
                {{-- <a href="#"
                    class="btn border hover-white bg-hover-primary mr-1 border-primary text-secondary font-weight-500">{{ trans('dashboard.actions.export') }}
                </a> --}}
                {{-- <a href="#"
                    class="btn border hover-white bg-hover-primary mr-1 border-primary text-secondary font-weight-500">{{ trans('dashboard.actions.import') }}
                </a> --}}
                @if ($action['create'])
                    <a href="{{ $action['create'] }}" class="btn btn-primary">{{ trans('dashboard.actions.add') }}
                        {{ $table_title }}
                    </a>
                @endif
            </div>
        </div>
        <div class="card mb-4 rounded-xl form-control-01">
            <div class="card-header bg-transparent p-4">
                <div class="row align-items-center">
                    <div class="col-md-4 col-12 mr-auto mb-md-0 mb-3">
                        <input type="text" placeholder="Search..." id="search" class="form-control bg-input border-0">
                    </div>
                    @if (Route::is('products.index'))
                        <div class="col-md-2 col-6 text-right">
                            <button class="btn btn-light btn-sm w-50" tabindex="0" aria-controls="example" type="button"
                                title="Change views" id="card-view"><span><i class="fa fa-th fa-fw"
                                        aria-hidden="true"></i></span></button>
                        </div>
                    @endif

                    <div class="col-md-3 col-6">
                        <select class="form-control bg-input border-0" id="rowsPerPage">
                            <option value="4">Show 4</option>
                            <option value="10" selected>Show 10</option>
                            <option value="15">Show 15</option>
                            <option value="20">Show 20</option>
                            <option value="-1">Show All</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-body p-4">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-hover align-middle table-nowrap mb-0 table-borderless">

                    </table>
                </div>
            </div>


        </div>

    </div>

@endsection
@section('script')
    @stack('script')
@endsection

@extends('app')
@section('title', $form_title)
@section('content')
    <div class="dashboard-page-content">
        <form action="{{ $action['url'] }}" method="post" enctype="multipart/form-data">
            @csrf
            @if ($action['type'] == 'edit')
                @method('put')
            @endif
            <div class="row mb-6">
                <div class="col-sm-6 mb-3 mb-sm-0">
                    <h2 class="fs-24 mb-0">{{ $form_title }}</h2>
                </div>
                <div class="col-sm-6 text-sm-right">
                    <a href="{{ $action['back'] }}"
                        class="btn border hover-white bg-hover-primary mr-1 border-primary text-secondary font-weight-500">{{ $table_title }}
                        {{ trans('dashboard.pages.list') }}</a>

                </div>
            </div>
            @yield('form')
            <div class="row">
                <div class="col-lg-7 col-12"></div>
                <div class="col-lg-5 col-12 text-right">
                    <a href="{{ $action['back'] }}"
                        class="btn border hover-white bg-hover-primary mr-1 border-primary text-secondary font-weight-500">{{ $table_title }}
                        {{ trans('dashboard.pages.list') }}</a>
                    <button type="submit" class="btn btn-primary">{{ $button['name'] }}</button>

                </div>
            </div>
        </form>
    </div>
@endsection

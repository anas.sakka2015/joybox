@extends('dashboard.components.list', [
    'table_title' => trans('dashboard.tasks.title'),
    'table_description' => trans('dashboard.tasks.description'),
    'action' => [
        'create' => route('tasks.create'),
    ],
])

@push('script')
    <script src="{{ asset('frontEnd/js/addLifeCycle.js') }}"></script>
    <script>
        DataTableOptions.ajax = {
            "url": '{{ route('tasks.index') }}',
        }


        DataTableOptions.columns = [{
                title: "{{ trans('dashboard.inputs.id') }}",
                data: "id",
                name: "id",
                orderable: false,
            },

            {
                data: 'title',
                name: "title",
                title: "{{ trans('dashboard.inputs.title') }}",
            },

            {
                data: 'deadline',
                name: "deadline",
                title: "{{ trans('dashboard.inputs.deadline') }}",
            },
            {
                data: 'created_at',
                name: "created_at",
                title: "{{ trans('dashboard.inputs.started_at') }}",
                render: function(data, type, JsonResultRow, meta) {
                    return `${formatDate(data)}`;
                }
            },
            {
                data: 'project',
                name: "project.title",
                title: "{{ trans('dashboard.inputs.project') }}",
                render: function(data, type, JsonResultRow, meta) {
                    return `<a href='${data ? '/projects/'+data.id : '#'}'>${data ? data.title : '---'}</a>`;
                }
            },
            {
                data: 'status',
                name: "status",
                title: "{{ trans('dashboard.inputs.status') }}",
                render: function(data, type, JsonResultRow, meta) {
                    var alertArray = [];
                    alertArray['new'] = 'info';
                    alertArray['in_progress'] = 'warning';
                    alertArray['done'] = 'success';
                    alertArray['canceled'] = 'danger';
                    alertArray['on_hold'] = 'dark';
                    if (data == 'done' || data == 'canceled')
                        return `<span
                    class="badge rounded-pill alert-${alertArray[data]}  text-capitalize fs-12">${data}</span>`;
                    return `<a class="change-status"
                    data-url="tasks/change_status/${JsonResultRow.id}" href="#"><span
                    class="badge rounded-pill alert-${alertArray[data]}  text-capitalize fs-12">${data}</span></a>`;
                }
            },
            {
                data: "action",
                name: 'action',
                orderable: false,
                searchable: false,
                className: 'text-center',
            }
        ];

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }
    </script>
@endpush

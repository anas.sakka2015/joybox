@extends('dashboard.components.details', [
    'table_title' => trans('dashboard.tasks.title'),
    'form_title' => trans('dashboard.actions.show') . ' ' . trans('dashboard.tasks.title'),
    'details_title' => trans('dashboard.tasks.title') . ' ' . trans('dashboard.inputs.details') . ' / ' . $data->title,
    'details_description' => trans('dashboard.tasks.description'),
    'action' => [
        'back' => route('tasks.index'),
    ],
])
@section('details')
    <div class="card mb-5 rounded-xl">
        <div class="card-header p-4 bg-transparent">
            <h4 class="fs-18 mb-0  font-weight-500">{{ trans('dashboard.actions.main_info') }}</h4>
        </div>
        <div class="card-body p-4">
            <div class="row mb-5">
                <div class="col-lg-6 col-6 mb-5">
                    <label for="title" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">
                        {{ trans('dashboard.inputs.title') }}</label>
                    <p>{{ $data->title }}</p>
                </div>
                <div class="col-lg-6 col-6 mb-5">
                    <label for="project_id" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">
                        {{ trans('dashboard.inputs.project') }}</label>
                    <p>{{ $data->project->title }}</p>
                </div>
                <div class="col-lg-6 col-6 mb-5">
                    <label for="status" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">
                        {{ trans('dashboard.inputs.status') }}</label>
                    <p>{{ $data->status }}</p>
                </div>
                <div class="col-lg-6 col-6 mb-5">
                    <label for="started_at" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">
                        {{ trans('dashboard.inputs.started_at') }}</label>
                    <p>{{ $data->created_at }}</p>
                </div>
                <div class="col-lg-6 col-6 mb-5">
                    <label for="deadline" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">
                        {{ trans('dashboard.inputs.deadline') }}</label>
                    <p>{{ $data->deadline }}</p>
                </div>
            </div>

            <div class="row my-5">
                <div class="col-12 mb-5">
                    <label for="description" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">
                        {{ trans('dashboard.inputs.description') }}</label>
                    <p>{{ $data->description }}</p>
                </div>

            </div>
        </div>
    </div>
@endsection

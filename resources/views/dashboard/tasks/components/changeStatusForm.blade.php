<form id="change-status" action="{{ route('tasks.updateStatus', $data->id) }}" method="post">
    @csrf
    @method('PUT')
    <div class="modal-header  border-0 p-6">
        <h5 class="modal-title" id="exampleModalLabel">{{ trans('dashboard.inputs.status') }}
            {{ trans('dashboard.inputs.lifeCycles') }}</h5>
        <button type="button" class="close opacity-10 fs-32 pt-1 position-absolute" data-dismiss="modal" aria-label="Close"
            style="right: 30px">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @csrf
    <div class="modal-body">
        <div class="my-2">
            <label for="content"
                class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required">{{ trans('dashboard.inputs.status') }}</label>
            <select data-style="form-control w-100 border rounded"
                class="form-control bg-input border-0 select @error('status') is-invalid @enderror" name="status"
                id="status">
                @foreach ($status as $item)
                    <option value="{{ $item }}" {{ $item == $data->status ? 'selected' : '' }}>
                        {{ $item }}</option>
                @endforeach
            </select>
            @error('status')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

    </div>

    <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary submit-status">{{ trans('dashboard.actions.save') }}</button>
    </div>
</form>

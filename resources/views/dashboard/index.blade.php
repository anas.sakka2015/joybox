@extends('app')
@section('title', 'dashboard')
@section('content')
    <div class="dashboard-page-content">
        <div class="row mb-6 align-items-center">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <h2 class="fs-24 mb-0">Dashboard</h2>
                <p class="mb-0">Whole data about your business here</p>
            </div>
            <div class="col-sm-6 text-sm-right">
                <a href="#" class="btn btn-primary"><i class="fal fa-file-plus"></i><span
                        class="d-inline-block ml-1">Create report</span>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xxl-3 mb-4">
                <div class="card rounded-xl">
                    <div class="card-body p-4">
                        <div class="media">
                            <div class="mr-3">
                                <span
                                    class="w-48px h-48px d-flex align-items-center justify-content-center fs-20 badge rounded-circle text-green bg-green-light">
                                    <i class="fas fa-usd-circle"></i>
                                </span>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-1 card-title fs-16">Projects</h6>
                                <span class="fs-24 d-block font-weight-500 text-primary lh-12">{{ $projects }}</span>
                                <span class="fs-14">Projects Count.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xxl-3 mb-4">
                <div class="card rounded-xl">
                    <div class="card-body p-4">
                        <div class="media">
                            <div class="mr-3">
                                <span
                                    class="w-48px h-48px d-flex align-items-center justify-content-center fs-20 badge rounded-circle text-success bg-success-light">
                                    <i class="fas fa-truck"></i>
                                </span>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-1 card-title fs-16">Tasks</h6>
                                <span class="fs-24 d-block font-weight-500 text-primary lh-12">{{ $tasks }}</span>
                                <span class="fs-14">Tasks counts</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-8">
                <div class="card rounded-xl p-4 mb-4">
                    <h5 class="card-title fs-16 mb-3">Task statistics</h5>
                    <div class="card-body p-0">
                        <canvas id="mychart" class="chartjs" data-chart-type="line"
                            data-chart-labels='["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]'
                            data-chart-options='{"elements":{"line":{"tension":0.3}},"plugins":{"legend":{"labels":{"usePointStyle":true}}},"scales":{"y":{"ticks":{"display":true},"grid":{"display":true,"drawBorder":false,"drawTicks":true}},"x":{"ticks":{"display":true},"grid":{"display":true,"drawBorder":false,"drawTicks":true}}}}'
                            data-chart-datasets='[{"label":"done","data":@json($taskStatistics['done']),"backgroundColor":"#2C78DC33","hoverBackgroundColor":"#2C78DC33","borderColor":"#2C78DC","hoverBorderColor":"#2C78DC","borderWidth":1,"fill":true},{"label":"In progress","data":@json($taskStatistics['in_progress']),"backgroundColor":"#04D18233","hoverBackgroundColor":"#04D18233","borderColor":"#04D182","hoverBorderColor":"#04D182","borderWidth":1,"fill":true},{"label":"canceled","data":@json($taskStatistics['canceled']),"backgroundColor":"#EF287830","hoverBackgroundColor":"#EF287830","borderColor":"#EF287391","hoverBorderColor":"#EF287391","borderWidth":1,"fill":true}]'
                            data-chart-additional-options='{"chatId":"mychart"}' height="265"></canvas>
                    </div>
                </div>
              
            </div>
            <div class="col-xl-4">

                <div class="card mb-4 p-4 rounded-xl">
                    <div class="card-body p-0">
                        <h5 class="card-title fs-16 mb-2">Projects</h5>
                        @foreach ($projectsPercents as $projectsPercent)
                            <span class="text-muted fs-12">{{ $projectsPercent->title }}</span>
                            <div class="progress mb-2">
                                <div class="progress-bar bg-success" role="progressbar"
                                    style="width: {{ $projectsPercent->percent }}%">
                                    {{ $projectsPercent->percent }}%
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

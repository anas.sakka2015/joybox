@extends('dashboard.components.form', [
    'table_title' => trans('dashboard.projects.title'),
    'form_title' => trans('dashboard.actions.update') . ' ' . trans('dashboard.projects.title'),
    'action' => [
        'back' => route('projects.index'),
        'url' => route('projects.update', $data->id),
        'type' => 'edit',
    ],
    'button' => [
        'name' => trans('dashboard.actions.save'),
    ],
])
@section('form')
    <div class="card mb-5 rounded-xl">
        <div class="card-header p-4 bg-transparent">
            <h4 class="fs-18 mb-0 font-weight-500">{{ trans('dashboard.inputs.enter') }}
                {{ trans('dashboard.projects.title') }}
                {{ trans('dashboard.inputs.infos') }}</h4>
        </div>
        <div class="card-body p-4">
            <div class="form-control-01">
                <div class="row">
                    <div class="col-lg-6 col-6 mb-5">
                        <label for="title"
                            class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required">{{ trans('dashboard.inputs.title') }}</label>
                        <input required type="text" name="title" id="title"
                            placeholder="{{ trans('dashboard.inputs.type_here') }}"
                            class="form-control bg-input border-0 @error('title') is-invalid @enderror"
                            value="{{ old('title') ? old('title') : $data->title }}">
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <hr class="my-3 p-0 font-weight-bold">
                <div class="row">
                    <div class="col-12 mb-5">
                        <label
                            class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required ">{{ trans('dashboard.inputs.description') }}</label>
                        <textarea required name="description" id="description" placeholder="Type here"
                            class="form-control bg-input border-0 @error('description') is-invalid @enderror" rows="4">{{ old('description') ? old('description') : $data->description }}</textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

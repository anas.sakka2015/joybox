@extends('dashboard.components.list', [
    'table_title' => trans('dashboard.projects.title'),
    'table_description' => trans('dashboard.projects.description'),
    'action' => [
        'create' => route('projects.create'),
    ],
])
@push('script')
    <script>
        DataTableOptions.ajax = {
            "url": '{{ route('projects.index') }}',
        }


        DataTableOptions.columns = [{
                title: "{{ trans('dashboard.inputs.id') }}",
                data: "id",
                name: "id",
                orderable: false,
            },
            {
                data: 'title',
                name: "title",
                title: "{{ trans('dashboard.inputs.title') }}",
            },
            {
                data: 'percent',
                name: "percent",
                title: "{{ trans('dashboard.inputs.percent') }}",
                render: function(data, type, JsonResultRow, meta) {
                    return `${data}%`;
                }

            },
            {
                data: 'tasks_count',
                name: "tasks_count",
                title: "{{ trans('dashboard.inputs.tasks_count') }}",

            },
            {
                data: 'is_done',
                name: "is_done",
                title: "{{ trans('dashboard.inputs.has_done') }}",
                render: function(data, type, JsonResultRow, meta) {
                    return `<a href="#" data-is-active='${data}'  data-url="projects/is_done/${JsonResultRow.id}" class='activate'>
                        <span class="badge rounded-pill alert-${data  ? 'success': 'danger'}
                        text-capitalize fs-12">
                            ${data ? 'Done': 'Not Yet' }</span></a>`;
                }

            },
            {
                data: "action",
                name: 'action',
                orderable: false,
                searchable: false,
                className: 'text-center',
            }
        ];
    </script>
@endpush

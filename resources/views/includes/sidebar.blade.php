<div class="db-sidebar bg-white">
    <nav class="navbar navbar-expand-xl navbar-light d-block px-0 header-sticky dashboard-nav py-0">
        <div class="sticky-area border-right">
            <div class="d-flex px-3 px-xl-6 w-100 border-bottom py-2">
                <a class="navbar-brand" href="{{ asset('frontEnd/index.html') }}">
                    <img src="{{ asset('frontEnd/images/logo-black.png') }}" alt="Glowing">
                </a>
                <div class="ml-auto d-flex align-items-center ">
                    <div class="d-flex align-items-center d-xl-none">
                        <div class="dropdown px-3">
                            <a href="#" class="dropdown-toggle d-flex align-items-center text-heading"
                                data-toggle="dropdown">
                                <div class="w-48px">
                                    <img src="{{ asset('frontEnd/images/avatar-2.png') }}" alt="Ronald Hunter"
                                        class="rounded-circle">
                                </div>
                                <span class="fs-13 font-weight-500 d-none d-sm-inline ml-2">
                                    Ronald Hunter
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">My Profile</a>
                                <a class="dropdown-item" href="#">My Profile</a>
                                <a href="{{ route('home.index') }}" class="dropdown-item">
                                    Logout
                                </a>
                            </div>
                        </div>
                        <div class="dropdown no-caret py-4 px-3 d-flex align-items-center notice mr-3">
                            <a href="#" class="dropdown-toggle text-heading fs-20 font-weight-500 lh-1"
                                data-toggle="dropdown">
                                <i class="far fa-bell"></i>
                                <span
                                    class="badge badge-primary badge-circle badge-absolute font-weight-bold fs-13">1</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <button class="navbar-toggler border-0 px-0" type="button" data-toggle="collapse"
                        data-target="#primaryMenuSidebar" aria-controls="primaryMenuSidebar" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
            <div class="collapse navbar-collapse bg-white" id="primaryMenuSidebar">
                <form class="d-block d-xl-none pt-5 px-3">
                    <div class="input-group position-relative bg-gray">
                        <input type="text" class="form-control border-0 bg-transparent pl-4 shadow-none"
                            placeholder="Search Item">
                        <div class="input-group-append fs-14 px-3 border-left border-2x ">
                            <button class="bg-transparent border-0 outline-none">
                                <i class="fal fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
                <ul class="list-group list-group-flush list-group-no-border w-100 p-3">
                    <li class="list-group-item px-0 py-0 sidebar-item mb-1">
                        <a href="{{ route('home.index') }}"
                            class="text-heading lh-1 sidebar-link py-2 px-3 px-xl-4 d-block {{ Route::is('home.index') ? 'navActive' : '' }}">
                            <span class="sidebar-item-icon d-inline-block mr-3 text-muted fs-18">
                                <i class="fas fa-home-lg-alt"></i>
                            </span>
                            <span class="sidebar-item-text">Home</span>
                        </a>
                    </li>

                    <li class="list-group-item px-0 py-0 sidebar-item mb-1 has-children">
                        <a href="#projects"
                            class="text-heading lh-1 sidebar-link d-flex align-items-center py-2 px-3 px-xl-4 {{ Route::is('projects.index') || Route::is('tasks.index') ? 'navActive' : '' }}"
                            data-toggle="collapse" aria-haspopup="true" aria-expanded="false">
                            <span class="sidebar-item-icon d-inline-block mr-3 text-muted fs-18">
                                <i class="fas fa-shopping-bag"></i>
                            </span>
                            <span class="sidebar-item-text">{{ trans('dashboard.pages.projects') }}</span>
                            <span class="d-inline-block ml-auto"><i class="fal fa-angle-down"></i></span>
                        </a>
                        <div class="menu-collapse {{ Route::is('projects.index') || Route::is('tasks.index') ? '' : 'collapse' }}"
                            id="projects">
                            <ul class="sub-menu list-unstyled">
                                <li class="sidebar-item">
                                    <a href="{{ route('projects.index') }}"
                                        class="text-heading lh-1 sidebar-link py-2 px-3 px-xl-4 d-block {{ Route::is('projects.index') ? 'navActive' : '' }}">

                                        <span class="sidebar-item-text">{{ trans('dashboard.projects.title') }}
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="{{ route('tasks.index') }}"
                                        class="text-heading lh-1 sidebar-link py-2 px-3 px-xl-4 d-block {{ Route::is('tasks.index') ? 'navActive' : '' }}">

                                        <span class="sidebar-item-text">{{ trans('dashboard.tasks.title') }}
                                        </span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

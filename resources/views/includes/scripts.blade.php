<script src="{{ asset('frontEnd/vendors/jquery.min.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/bootstrap/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/slick/slick.min.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/counter/countUp.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/hc-sticky/hc-sticky.min.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/jparallax/TweenMax.min.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/mapbox-gl/mapbox-gl.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/isotope/isotope.js') }}"></script>
<script src="{{ asset('frontEnd/vendors/chartjs/chart.min.js') }}"></script>

<script src="{{ asset('frontEnd/js/theme.js') }}"></script>

{{-- sweetalert2 --}}
<script src="{{ asset('frontEnd/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>

{{-- alertify --}}
<script src="{{ asset('frontEnd/alertifyjs/alertify.min.js') }}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

{{-- Delete alert  --}}
<script src="{{ asset('frontEnd/js/deleteAlert.js') }}"></script>

{{-- Activate alert  --}}
<script src="{{ asset('frontEnd/js/activateAlert.js') }}"></script>

<script>
    @if (session()->has('success'))
        localStorage.setItem("success", "{{ session()->get('success') }}");
    @endif

    if (localStorage.getItem("success")) {
        alertify.success(localStorage.getItem("success"));
        localStorage.clear();
    }
</script>
@if ($errors->any())
    <script>
        alertify.error('Something went wrong !');
    </script>
@endif

{{-- Data table --}}
<script src="{{ asset('frontEnd/dataTable/datatables.min.js') }}"></script>
<script src="{{ asset('frontEnd/js/dataTable.js') }}"></script>

{{-- Dropify --}}
<script src="{{ asset('frontEnd/dropify/dist/js/dropify.min.js') }}"></script>
<script src="{{ asset('frontEnd/js/dropify.js') }}"></script>

{{-- Light box --}}
<script src="{{ asset('frontEnd/lightBox/dist/js/lightbox.min.js') }}"></script>
<script src="{{ asset('frontEnd/js/lightbox.js') }}"></script>

{{-- Color selector --}}
<script src="{{ asset('frontEnd/js/colorSelector.js') }}"></script>

{{-- CK Editor --}}
{{-- <script src="{{ asset('frontEnd/CKEditor5/ckeditor.js') }}"></script> --}}



{{-- <script>
    const buttons = document.querySelectorAll(".btn-group__item");
    buttons.forEach((button) => {
        button.addEventListener("click", () => {
            // do some action according to button

            // show success feedback
            button.classList.add("btn-group__item--active");
            setTimeout(() => {
                button.classList.remove("btn-group__item--active");
            }, 600);
        });
    });
</script> --}}
{{-- <script>
    $.lightbox({
        beforeOpen: function() {
            document.body.style.overflow = 'hidden';
        },
        afterEnd: function() {
            document.body.style.overflow = '';
        }
    });
</script> --}}

<script src="{{ asset('frontEnd/js/inputNumberFormate.js') }}"></script>

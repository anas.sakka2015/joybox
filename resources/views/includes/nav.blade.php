<header class="main-header bg-white position-relative d-none d-xl-block">
    <div class="container-fluid">
        <nav class="navbar navbar-light py-0 row no-gutters px-3 px-lg-0">
            <div class="col-md-4 px-0 px-md-6 order-1 order-md-0 form-control-01">

            </div>
            <div class="col-md-6 d-flex flex-wrap justify-content-md-end align-items-center order-0 order-md-1">

                <div class="dropdown pl-2 py-2">
                    <a href="#"
                        class="dropdown-toggle text-heading pr-3 pr-sm-6 d-flex align-items-center justify-content-end"
                        data-toggle="dropdown">
                        <div class="w-40px">
                            <img src="{{ asset('frontEnd/images/avatar-2.png') }}" alt="Ronald Hunter"
                                class="rounded-circle">
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right w-100">
                        <a href="{{ route('home.index') }}" class="dropdown-item">
                            Logout
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>

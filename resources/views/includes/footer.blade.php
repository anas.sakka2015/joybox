<footer class="pt-3 pb-6 footer-dashboard mt-auto text-center">
    <div class="copyright">
        Developed By <span class="prosoft">Anas<span></span></span> Sakka
        CREATIVE SOLUTIONS,
        <script>
            document.write(new Date().getFullYear());
        </script>
        <span class="text-sm-center mx-1"> All Rights Reserved.</span>
    </div>
</footer>

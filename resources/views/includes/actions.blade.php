<a href="{{ route($routeName . '.show', $row) }}" class=" text-warning mx-2 fs-15"><i class="fas fa-eye"></i></a>
@if (!isset($hideActions['isUpdate']))
    <a class=" text-success fs-15 mr-2" href="{{ route($routeName . '.edit', $row) }}"><i class="fas fa-edit"></i>
    </a>
@endif
@if (!isset($hideActions['isDelete']))
    <a class=" text-primary fs-15 text-danger  delete" data-url="{{ route($routeName . '.destroy', $row) }}"
        href="#"><i class="fas fa-trash"></i> </a>
@endif

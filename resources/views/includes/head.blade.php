<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Glowing Shop Html Template">
<meta name="author" content="">
<meta name="generator" content="Jekyll">

<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@">
<meta name="twitter:creator" content="@">
<meta name="twitter:title" content="Dashboard">
<meta name="twitter:description" content="Glowing Shop Html Template">
<meta name="twitter:image" content="{{ asset('frontEnd/images/logo_01.png') }}">

<meta property="og:url" content="dashboard.html">
<meta property="og:title" content="Dashboard">
<meta property="og:description" content="Glowing Shop Html Template">
<meta property="og:type" content="website">
<meta property="og:image" content="{{ asset('frontEnd/images/logo_01.png') }}">
<meta property="og:image:type" content="{{ asset('frontEnd/image/png') }}">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="stylesheet" href="{{ asset('frontEnd/vendors/fontawesome-pro-5/css/all.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/bootstrap-select/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/slick/slick.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/magnific-popup/magnific-popup.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/animate.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/mapbox-gl/mapbox-gl.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/fonts/font-phosphor/css/phosphor.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/fonts/tuesday-night/stylesheet.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/fonts/butler/stylesheet.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/vendors/fonts/a-antara-distance/stylesheet.min.css') }}">

<link rel="stylesheet" href="{{ asset('frontEnd/css/themes.css') }}">

<link rel="icon" href="{{ asset('frontEnd/images/favicon.jpg') }}">

<link rel="stylesheet" href="{{ asset('frontEnd/css/myCss.css') }}">

{{-- alertify --}}
<link rel="stylesheet" href="{{ asset('frontEnd/alertifyjs/css/alertify.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontEnd/alertifyjs/css/themes/default.min.css') }}">

{{-- Data Table --}}
<link href="{{ asset('frontEnd/dataTable/datatables.min.css') }}" rel="stylesheet" />

{{-- Dropify --}}
<link href="{{ asset('frontEnd/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" />

{{-- Light box --}}
<link href="{{ asset('frontEnd/lightBox/dist/css/lightbox.min.css') }}" rel="stylesheet" />

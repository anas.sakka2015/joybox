<!doctype html>
<html lang="en">

<head>
    @include('includes.head')

    <title>{{ config('app.name') }} | Dashboard | @yield('title')</title>
</head>

<body>
    <div class="wrapper dashboard-wrapper">
        <div class="d-flex flex-wrap flex-xl-nowrap">
            @include('includes.sidebar')
            <div class="page-content">
                @include('includes.nav')
                <main id="content" class="bg-gray-01 d-flex flex-column main-content">
                    @yield('content')

                    @include('includes.footer')
                </main>
            </div>
        </div>
    </div>
    @include('dashboard.tasks.modals.changeStatus')

    @include('includes.scripts')


    @yield('script')

    <script>
        var table = $('#dataTable').DataTable(DataTableOptions);

        function tableDraw(tableFn, id) {

            $('#search').on('keyup click', function() {
                tableFn.search($(this).val()).draw();
            });
            $('#rowsPerPage').on('change', function() {
                let row = $(this).val()
                tableFn.page.len(row).draw();
            });
            tableFn.on('order.dt search.dt draw.dt', function() {
                let i = tableFn.page.info().length * tableFn.page() + 1;

                tableFn.cells(null, 0, {
                    search: 'applied',
                    order: 'applied'
                }).every(function(cell) {
                    this.data(i++);
                });
            }).draw();
            $('#card-view').on('click', function() {
                tableFn.button('.btn-sm').trigger();

            });
        }
        tableDraw(table);
    </script>

</body>

</html>

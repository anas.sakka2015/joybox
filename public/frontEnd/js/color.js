var btnPlusColor = '<div class="col-lg-3 col-6 plus-div">' +
'<div class="middle-btn text-center">' +
'<button type="button" class="btn btn-outline-primary btn-circle btn-xl create-color" ><i class="fa fa-plus"></i></button>' +
'</div></div>';
var errorDiv = '<div class="invalid-feedback d-block" role="alert">' +
'<strong>This field is required</strong>' +
'</div>';
$("#color-row").on("click", '.create-color', function() {
var canCreate = false;
var createColorDiv = $(this);

$(".color-image").each(function() {
    var colorCode = $(this).closest('.input-upload').children('.row').children('.color-code-div').children(
        '.border-0');
    var smell = $(this).closest('.input-upload').children('.row').children('.smell-div').children(
                '.border-0');
    var quantity = $(this).closest('.input-upload').children('.row').children('.quantity-div').children(
                '.border-0');
    if ($(this).closest('.input-upload').find('.has-preview').length !== 0 && colorCode.find(
            ":selected").val() != "" &&  smell.find(
                ":selected").val() != "" && quantity.val()!= "") {
        canCreate = true;
    } else {
        $("select.select").each(function() {
            if ($(this).find(":selected").val() == "") {
                if ($(this).closest('div .dropdown').children('.invalid-feedback')
                    .length == 0)
                    $(this).closest('.dropdown').append(errorDiv);
                    $(this).addClass('is-invalid');
            }
        });
        $("#color-row input[type='number']").each(function() {
            if ($(this).val() == "") {
                $(this).addClass('is-invalid');
                if ($(this).closest('div').children('.invalid-feedback')
                .length == 0)
                $(this).closest('div').append(errorDiv);
            }
        });
        canCreate = false;
        return false;
    }
});
if ($('.color-image').length == 0) {
    canCreate = true;
}
if (canCreate == true) {

    var colDiv = '<div class="col-lg-6 col-6">' +
        '<div class="input-upload mb-5">' +
        '<div class="row">' +
        '<div class="col-6">' +
        '<label class = "mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required" >Color Image' +
        '</label> </div >' +
        '<div class="col-6"> <div class=" text-right">' +
        '<button type = "button" class = "btn btn-default btn-circle text-danger btn-sm w-25 removeCard" ><i class="fa fa-times"></i></button></div></div></div>' +
        '<input required type="file" name="colors[' + count +
        '][image_path]" class="dropify color-image"  data-min-width="670"'+
        'data-max-width="690" data-min-height="670" data-max-height="690">' +
        '<div class="row"><div class="col-lg-6 col-12 mt-2 color-code-div">' +
        '<label  class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required" >Colors</label>' +
        ' <select required id="select-' + count +
        '" class="form-control border-0 select color-select bg-input "' +
        'data-colorselect name="colors[' + count + '][color_id]" >' +
        '<option value class="d-none">NON</option></select>' +
        '</div>'+
        '<div class="col-lg-6 col-12 mt-2 smell-div">'+
        '<label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required">Smells</label>'+
        '<select id="select-smell-' + count +
        '" required class="form-control bg-input border-0 select " data-style="form-control w-100 border rounded" name="colors['+count+'][smell_id]">'+
        ' <option value selected>Select Smells</option>'+
        '</select>'+
        '</div>'+
        '<div class="col-lg-12 col-12 mt-2 quantity-div">'+
        '<label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required">Quantity</label>'+
        '<input type="number" class="form-control bg-input border-0 " required name="colors['+count+'][quantity]"'+
       'placeholder="Type here"></div>'+
        '</div></div></div>';

    createColorDiv.closest('.plus-div').remove();
    document.getElementById("color-row").insertAdjacentHTML('beforeend', colDiv);
    $.each(colorsItems, function(obj) {
        $('#select-' + count).append('<option value="' + this.id + '" data-code="' + this.code +
            '" >' + this.title_en + '</option>')
    });
    $.each(smellsItems, function(obj) {
        $('#select-smell-' + count).append('<option value="' + this.id + '">' + this.title_en + '</option>')
    });
    document.getElementById("color-row").insertAdjacentHTML('beforeend', btnPlusColor);
    // console.log($('.color-image'));
    $('.color-image').last().dropify();
    $('[data-colorselect]').last().selectpicker();
    $('#select-smell-'+ count).last().selectpicker();
    $('[data-colorselect]').last().colorSelect();

    count++;
}
});

function validationSelect(elment) {
    $("select.select").each(function() {
        if ($(this).find(":selected").val() == "") {
            if ($(this).closest('div .dropdown').children('.invalid-feedback')
                .length == 0)
                $(this).closest('.dropdown').append(errorDiv);
        }else{
            $(this).closest('div .dropdown').children('.invalid-feedback').remove();
        }
    });
    $("#color-row input[type='number']").each(function() {
        if ($(this).val() == "") {
            if ($(this).closest('div').children('.invalid-feedback')
            .length == 0)
            $(this).closest('div').append(errorDiv);
            $(this).addClass('is-invalid');
        }
        else{
            $(this).closest('div').children('.invalid-feedback').remove();
            $(this).removeClass('is-invalid');
        }
    });
}
$('#color-row').on('change','select.select', function() {
validationSelect($(this));
});
$('#color-row').on('change','.bg-input', function() {
    validationSelect($(this));
    });
$("#color-row").on("click", '.removeCard', function() {
var elment = $(this).closest('.input-upload').closest('.col-lg-6');
elment.remove();
});

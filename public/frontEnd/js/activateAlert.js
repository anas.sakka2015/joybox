$(document).on("click", ".activate", function () {
    var url = $(this).attr("data-url");
    var is_active = $(this).attr("data-is-active");
    var element = $(this);
    Swal.fire({
        title:
            is_active == 0
                ? "Are you sure you want to done this record ?"
                : "Are you sure you want to undone this record ?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4e7661",
        cancelButtonColor: "#d33",
        confirmButtonText:
            is_active == 0
                ? "Yes, Make The Project Done!"
                : "Yes, Make The Project Not Done Yet!",
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: url,
                type: "PUT",
                success: function (data) {
                    table.draw();
                    alertify.success(data.message);
                },
                error: function (data) {
                    var errors = data.responseJSON;
                    console.log(errors);
                    alertify.error("Something went wrong !");
                },
            });
        }
    });
});

$(document).on("click", ".onlyOneActive", function () {
    var url = $(this).attr("data-url");
    var is_active = $(this).attr("data-is-active");
    var element = $(this);
    var product_id = $(this).attr("data-product-id");
    Swal.fire({
        title:
            is_active == 1
                ? "Are you sure you want to de activate this record ?"
                : "Are you sure you want to activate this record ?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4e7661",
        cancelButtonColor: "#d33",
        confirmButtonText:
            is_active == 1 ? "Yes, De activate  it!" : "Yes, Activate  it!",
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: url,
                type: "PUT",
                success: function (data) {
                    if (is_active == 1) {
                        element.removeClass("text-success");
                        element.children().removeClass("fa-toggle-on");
                        element.addClass("text-danger");
                        element.children().addClass("fa-toggle-off");
                    } else {
                        $(".onlyOneActive")
                            .data("is-active", 1)
                            .each(function () {
                                if (
                                    $(this).data("is-active") == 1 &&
                                    $(this).data("product-id") == product_id
                                ) {
                                    $(this).removeClass("text-success");
                                    $(this)
                                        .children()
                                        .removeClass("fa-toggle-on");
                                    $(this).addClass("text-danger");
                                    $(this)
                                        .children()
                                        .addClass("fa-toggle-off");
                                    $(this).attr("data-is-active", 0);
                                }
                            });
                        element.removeClass("text-danger");
                        element.children().removeClass("fa-toggle-off");
                        element.addClass("text-success");
                        element.children().addClass("fa-toggle-on");
                    }
                    $(element).attr("data-is-active", is_active == 1 ? 0 : 1);
                    alertify.success(data.message);
                },
                error: function (data) {
                    var errors = data.responseJSON;
                    console.log(errors);
                    alertify.error("Something went wrong !");
                },
            });
        }
    });
});

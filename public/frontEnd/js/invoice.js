function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}

function downloadPdf(pdfName) {
    var divToExport = document.getElementById('DivIdToPrint');

    // Create a new jsPDF instance
    var doc = new jsPDF();

    // Use the html2canvas library to render the Bootstrap div as an image
    html2canvas(divToExport).then(function(canvas) {
        // Add the image to the PDF document
        var imgData = canvas.toDataURL('image/png');
        doc.addImage(imgData, 'PNG', 10, 10, 180, 240);

        // Save the PDF document
        doc.save(pdfName + '.pdf');
    });
}
$(".life-cycle-table").hide();
$(".changeContent").on("click", function() {
    $(".item-table").toggle(500);
    $(".life-cycle-table").toggle(500);
    $(".cash-details").toggle(500);
    $(this).text(function(i, text) {
        return text == "Life cycles" ? "Item info" : "Life cycles";
    })
});

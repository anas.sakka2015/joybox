var btnPlus = '<div class="col-lg-3 col-6 plus-div">' +
'<div class="middle-btn text-center">' +
'<button type="button" class="btn btn-outline-primary btn-circle btn-xl create-gallery" ><i class="fa fa-plus"></i></button>' +
'</div></div>';
var errorSpan = '<span class="invalid-feedback" role="alert">' +
    '<strong>This field is required</strong>' +
    '</span>';
$("#gallery-row").on("click", '.create-gallery', function() {
var canCreate = false;
var createGalleryDiv = $(this);
$(".gallery").each(function() {
    var titleGalleryEn = $(this).closest('.input-upload').children('.title-inputs').children(
        '.titleEn').children('.bg-input');
    var titleGalleryFr = $(this).closest('.input-upload').children('.title-inputs').children(
        '.titleFr').children('.bg-input');
    var titleGalleryAr = $(this).closest('.input-upload').children('.title-inputs').children(
        '.titleAr').children('.bg-input');
    var titleGalleryRu = $(this).closest('.input-upload').children('.title-inputs').children(
        '.titleRu').children('.bg-input');
    if ($(this).closest('.input-upload').find('.has-preview').length !== 0 && titleGalleryEn
        .val() != "" && titleGalleryFr.val() !=
        "" && titleGalleryAr.val() != "" && titleGalleryRu.val() != "") {
        canCreate = true;


    } else {
        $("input[type='text']").each(function() {
            if ($(this).val() == "") {
                $(this).addClass('is-invalid');
                if($(this).closest('div').children('span.invalid-feedback').length==0)
                $(this).after(errorSpan);

            }
        });
        canCreate = false;
        return false;
    }
});
if ($('.gallery').length == 0) {
    canCreate = true;
}
if (canCreate == true) {
    var colDiv = '<div class="col-lg-6 col-6">' +
        '<div class="input-upload mb-5">' +
        '<div class="row">' +
        '<div class="col-6">' +
        '<label class = "mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required" >Image' +
        '</label> </div >' +
        '<div class="col-6"> <div class=" text-right">' +
        '<button type = "button" class = "btn btn-default btn-circle text-danger btn-sm w-25 removeCard" ><i class="fa fa-times"></i></button></div></div></div>' +
        '<input required type="file" name="gallery[' + count +
        '][image]" class="dropify gallery " data-min-width="670"'+
        'data-max-width="690" data-min-height="670" data-max-height="690">' +
        '<div class="row title-inputs">' +
        '<div class="col-lg-6 col-12 titleEn"><label  class = "mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required" >Title IN ENGLISH</label>' +
        '<input required type="text"  name="gallery[' + count +
        '][title_en]"   class="form-control bg-input border-0" placeholder = "Type Hear" >' +
        '</div>' +
        '<div class="col-lg-6 col-12 titleFr"><label  class = "mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required" >Title IN FRENCH</label>' +
        '<input required type="text"  name="gallery[' + count +
        '][title_fr]"   class="form-control bg-input border-0" placeholder = "Type Hear" >' +
        '</div>' +
        '<div class="col-lg-6 col-12 titleAr"><label  class = "mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required" >Title IN ARABIC</label>' +
        '<input required type="text"  name="gallery[' + count +
        '][title_ar]"   class="form-control bg-input border-0" placeholder = "Type Hear" >' +
        '</div>' +
        '<div class="col-lg-6 col-12 titleRu"><label  class = "mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase required" >Title IN RUSSIAN</label>' +
        '<input required type="text"  name="gallery[' + count +
        '][title_ru]"   class="form-control bg-input border-0" placeholder = "Type Hear" >' +
        '</div>' +
        '</div></div></div>';
    createGalleryDiv.closest('.plus-div').remove();
    document.getElementById("gallery-row").insertAdjacentHTML('beforeend', colDiv);
    document.getElementById("gallery-row").insertAdjacentHTML('beforeend', btnPlus);
    $('.gallery').last().dropify();
    count++;
}
});
$('#gallery-row').on('change', '.bg-input', function() {
$("#gallery-row .bg-input").each(function() {
    if ($(this).val() != '') {
        $(this).removeClass('is-invalid');
        $(this).find("span.invalid-feedback").remove();
    } else {
        $(this).addClass('is-invalid');
        if($(this).closest('div').children('span.invalid-feedback').length==0)
        $(this).after(errorSpan);
    }
});
});
$("#gallery-row").on("click", '.removeCard', function() {
var elment = $(this).closest('.input-upload').closest('.col-lg-6');
elment.remove();
});

$('.card-body').on("keyup", "input[type='number']",
function() {

    var max = parseInt($(this).attr('max'));
    var min = parseInt($(this).attr('min'));
    let v = parseInt($(this).val());
    $(this).val(v);
    if (v < min) $(this).val(min);
    if (v > max) $(this).val(max);
});

let DataTableOptions = {
    responsive: false,
    bDestroy: true,
    processing: true,
    serverSide: true,
    filter: true,
    "language": {
        "paginate": {
          "next": "<i class='fas fa-angle-right'></i>",
          "previous": "<i class='fas fa-angle-left'></i>",
        }
      },
    "dom":"rt" +
    "<'row'<'col-sm-4'i><'col-sm-8'p>>",
   "sScrollY": "100%",

    order: [
        [0, "desc"]
    ],
    columnDefs: [
        {
            searchable: false,
            orderable: false,
            targets: 0,
        },
    ],
    oLanguage: {
        sLengthMenu: "_MENU_",
        sSearch: "",
    },
    aLengthMenu: [
        [4, 10, 15, 20, -1],
        [4, 10, 15, 20, "All"]
    ],
    iDisplayLength: 10,
    select: {
        style: "multi"
    },
    bInfo: true,
    pageLength: 10,
    lengthChange: true,
    width: "100%",

    fnDrawCallback: function(oSettings) {
        if ((oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) || oSettings._iDisplayLength == -1) {
            $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
        } else {
            $(oSettings.nTableWrapper).find('.dataTables_paginate').show();
        }
        var api = this.api();
        var $table = $(api.table().node());

        if ($table.hasClass('cards')) {

           // Create an array of labels containing all table headers

           $('.dataTables_scrollBody tr').each(function(){
             $(this).attr('class', 'card rounded-xl card-product mb-4 text-center');
             $(this).children('td').children('a').addClass('w-100');
             $(this).children('td').children('a').children('div').removeClass('w-60px');
             $(this).children('td').children('a').children('div').removeClass('mr-4');
             $(this).children('td').children('a').children('div').addClass('w-100');
             $(this).children('td').children('a').children('div').addClass('text-center');
             $(this).children('td').children('a').children('div').children('img').addClass('w-200-px');
             $(this).children('td').children('a').children('div').children('img').addClass('h-200-px');
            });
           document.querySelectorAll('.dataTables_scrollBody tr td.sorting_1').forEach((id) => id.remove());
           $('.dataTables_scroll .dataTables_scrollHead').addClass('d-none');

        } else {
           // Remove data-label attribute from each cell
           $('.dataTables_scroll .dataTables_scrollHead').removeClass('d-none');
           $('tbody td', $table).each(function () {
              $(this).removeAttr('data-label');
           });

           $('tbody tr', $table).each(function () {
              $(this).height('auto');
           });
        }
    },
};



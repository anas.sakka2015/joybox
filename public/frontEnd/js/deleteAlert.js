$(document).on("click", ".delete", function () {
    var url = $(this).attr("data-url");
    var element = $(this);
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4e7661",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: url,
                type: "DELETE",
                success: function () {
                    table.draw();

                    alertify.success("Deleted Successfully !");
                },
                error: function (data) {
                    var errors = data.responseJSON;
                    console.log(errors);
                    alertify.error("Something went wrong !");
                },
            });
        }
    });
});

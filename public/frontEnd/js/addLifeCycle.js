var errorDiv =
    '<span class="invalid-feedback d-block" role="alert">' +
    '<strong class="error"></strong>' +
    "</span>";
$(document).on("click", ".change-status", function () {
    var url = $(this).attr("data-url");
    var element = $(this);
    $.ajax({
        url: url,
        type: "get",
        success: function (data) {
            var modal = $(".change-status-modal");
            $(".modal-content").html(data);
            // Show the modal
            modal.modal("show");
            $("#order_status").selectpicker();
        },
        error: function (data) {
            var errors = data.responseJSON;
            console.log(errors);
            alertify.error("Something went wrong !");
        },
    });
});
$(document).on("submit", "form#change-status", function (e) {
    e.preventDefault();
    $(".is-invalid").removeClass("is-invalid");
    $.each($(".invalid-feedback"), function (key, value) {
        if ($(this).length != 0) {
            $(this).remove();
        }
    });
    var url = $(this).attr("action");
    var request = $(this).serialize();
    $.ajax({
        url: url,
        type: "put",
        data: request,
        success: function (data) {
            var modal = $(".change-status-modal");
            modal.modal("hide");
            alertify.success(data.message);
            table.draw();
        },
        error: function (data) {
            var errors = data.responseJSON;
            alertify.error("Something went wrong !");
            $.each(errors.errors, function (key, value) {
                if (
                    $("#" + key)
                        .closest("div.my-2")
                        .children(".invalid-feedback").length == 0
                )
                    $("#" + key)
                        .closest("div.my-2")
                        .append(errorDiv);
                $("#" + key)
                    .closest("div.my-2")
                    .children(".invalid-feedback")
                    .children("strong.error")
                    .html(value);
                $("#" + key).addClass("is-invalid");
            });
        },
    });
});
